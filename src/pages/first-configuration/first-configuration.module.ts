import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FirstConfigurationPage } from './first-configuration';

@NgModule({
  declarations: [
    FirstConfigurationPage,
  ],
  imports: [
    IonicPageModule.forChild(FirstConfigurationPage),
  ],
})
export class FirstConfigurationPageModule {}
