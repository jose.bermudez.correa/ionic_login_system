import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { Component, Input } from '@angular/core';
import {IonSimpleWizard} from '../ion-simple-wizard/ion-simple-wizard.component';
import {IonSimpleWizardStep} from '../ion-simple-wizard/ion-simple-wizard.step.component';

/**
 * Generated class for the FirstConfigurationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-first-configuration',
  templateUrl: 'first-configuration.html',
})
export class FirstConfigurationPage {

  step: number;
  stepCondition: any;
  stepDefaultCondition: any;
  currentStep: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,  public evts: Events) {

    this.step = 1;//The value of the first step, always 1
    this.stepCondition = false;//Set to true if you don't need condition in every step
    this.stepDefaultCondition = this.stepCondition;//Save the default condition for every step
    //You can subscribe to the Event 'step:changed' to handle the current step
    this.evts.subscribe('step:changed', step => {
      //Handle the current step if you need
      this.currentStep = step;
      //Set the step condition to the default value
      this.stepCondition = this.stepDefaultCondition;
    });
    this.evts.subscribe('step:next', () => {
      //Do something if next
      console.log('Next pressed: ', this.currentStep);
    });
    this.evts.subscribe('step:back', () => {
      //Do something if back
      console.log('Back pressed: ', this.currentStep);
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FirstConfigurationPage');
  }

}
