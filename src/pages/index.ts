// The page the user lands on after opening the app and without a session
export const FirstRunPage = 'TutorialPage';

// The main page the user will see as they use the app over a long period of time.
// Change this if not using tabs
//export const MainPage = 'TabsPage';

export const MainPage = 'FirstConfigurationPage';

// The initial root pages for our tabs (remove if not using tabs)
export const Tab1Root = 'ListMasterPage';
export const Tab2Root = 'SearchPage';
export const Tab3Root = 'SettingsPage';

// The page the user lands to login
export const LoginPage = 'LoginPage';

// The page the user lands to welcome
export const WelcomePage = 'WelcomePage';

// The confuration Page
export const FirstConfigurationPage = 'FirstConfigurationPage';
