import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFireAuth  } from '@angular/fire/auth';


import { IApi } from '../api/interface';

/*
  Generated class for the FirebaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class Firebase implements IApi {

  constructor(private firebaseAuth: AngularFireAuth) {
    console.log('Hello FirebaseProvider Provider');
  }

  /**
   * Allow to sing in using and email and password
   * @param {accountInfo} color - The shoe's color.
   */
  login(accountInfo: any) {
    return this.firebaseAuth.auth.signInWithEmailAndPassword(
        accountInfo.email,
        accountInfo.password
    );
  }

  signup(accountInfo: any) {
    return this.firebaseAuth.auth.createUserWithEmailAndPassword(
        accountInfo.email,
        accountInfo.password
    );
  }

  authState() {
      return this.firebaseAuth.user;
  }

  logout() {
    this.firebaseAuth.auth.signOut();
  }

}
