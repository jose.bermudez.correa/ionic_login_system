import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth  } from '@angular/fire/auth';

import { IApi } from './interface';

/*
  Generated class for the FirebaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FirebaseProvider implements IApi {

  constructor(private firebaseAuth: AngularFireAuth) {
    console.log('Hello FirebaseProvider Provider');
  }

  /**
   * Allow to sing in using and email and password
   * @param {accountInfo} color - The shoe's color.
   */
  login(accountInfo: AccountModel.Account) {
    console.log(accountInfo);
    return accountInfo;
    /*return this.afAuth.auth.signInWithEmailAndPassword(
        accountInfo.email,
        accountInfo.password
    );*/
  }

}
